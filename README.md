# README #

Contains information on goals/project ideas.

sites:

info.cern.ch/hypertext/WWW/TheProject.html
www.google.com
en.wikipedia.org/wiki/South_African_labour_law
www.reddit.com
www.yahoo.com
www.youtube.com/watch?v=dQw4w9WgXcQ
www.cs.unm.edu/~forrest/publications/acsac08.pdf
www.nyu.edu
www.wfu.edu
www.soundcloud.com/flume/lorde-tennis-court-flume-remix

command:

strace -o ~/grive/Research/attribute_mining/[etc.] wget -e robots=off --wait 1 --page-requisites [link]

soundcloud:
 - https://soundcloud.com/flume/lorde-tennis-court-flume-remix
 - https://soundcloud.com/favelamusic/easy-yoke
 - https://soundcloud.com/starslingeruk/h-town-they-like-it-slow-star

cern:
 - http://info.cern.ch/hypertext/WWW/TheProject.html
 - http://info.cern.ch/hypertext/WWW/Help.html
 - http://info.cern.ch/hypertext/WWW/Technical.html


//TODO:

group websites into categories, i.e. university sites, streaming sites,
news sites, wikipedia pages (lists vs articles), etc.

can it differentiate between website types?

search site terms:
 - homepage
 - wake forest
 - nyu
 - linux
 - computer science

university sites:
 - wfu.edu
 - nyu.edu
 - duke.edu
 - unc.edu
 - utexas.edu
 - berkeley.edu
 - usc.edu
 - ucla.edu
 - cornell.edu
 - uchicago.edu

valgrind on server:
 - run strace on valgrind runs
 - see if you can see the difference 
   between high and low memory usage






